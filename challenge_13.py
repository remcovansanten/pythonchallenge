'''
http://www.pythonchallenge.com/pc/return/disproportional.html

link on 5 leads to: http://www.pythonchallenge.com/pc/phonebook.php
This gives us a php with XML output...
hmmm... let's think: xml=rpc and discover the services...

'''
import xmlrpc.client

def challenge13():
    # See what services are available...
    with xmlrpc.client.ServerProxy("http://www.pythonchallenge.com/pc/phonebook.php") as proxy:
        for method in proxy.system.listMethods():
            print(method)
            print(proxy.system.methodHelp(method))
    # Ah, I see: phone
    # assignment: call the evil one
    # From the previous challenge: Bert is evil
    # So.. Let's call Bert :-)
    print("Bert's phone number is: %s" % str(proxy.phone("bert")))
    # reply: Bert's phone number is: He is not the evil
    print("Evil's phone number is: %s" % str(proxy.phone("evil")))
    # So who is evil?
    # literaly: "that evil"??
    print("that evil's phone number is: %s" % str(proxy.phone("that evil")))
    # No, it must be Bert... wait.. Did I miss a capital.... really...
    print("Bert's phone number is: %s" % str(proxy.phone("Bert")))
    # Kazjing... I found the solution
    # although: Not quite there yet, but must be close.
    #let's lose the 555-
    if __name__ == '__main__':
        solution = 'ITALY'
        print(f'Challenge 13 solution is: {solution}')
        print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")
        print("are we sure? reply is SMALL letters.")
        print("let's try: 'italy' .. as this one is all about CAPS or not to caps")
        solution = 'italy'
        print(f'Challenge 13 solution is: {solution}')
        print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")
    

if __name__ == '__main__':
    challenge13()
