# http://www.pythonchallenge.com/pc/def/linkedlist.php
'''
<html>
<head>
  <title>follow the chain</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<!-- urllib may help. DON'T TRY ALL NOTHINGS, since it will never 
end. 400 times is more than enough. -->
<center>
<a href="linkedlist.php?nothing=12345"><img src="chainsaw.jpg" border="0"/></a>
<br><br><font color="gold"></center>
Solutions to previous levels: <a href="http://wiki.pythonchallenge.com/"/>Python Challenge wiki</a>.
<br><br>
IRC: irc.freenode.net #pythonchallenge
</body>
</html>

'''
def challenge4():
  import urllib3
  http = urllib3.PoolManager()
  url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing='
  param = '12345'
  solution =''
  for i in range(400):
      r = http.request('GET', url+param)
      temp = (r.data.decode('utf-8'))
      if temp.find('.html') >0 :
        solution = temp
        break
      param = (r.data.decode('utf-8')[24:])
  print('Challenge 4 solution is: ',end = '')
  print(solution)
  print(f"\nNext url is http://www.pythonchallenge.com/pc/def/{solution}.html")

if __name__ == '__main__':
    challenge4()