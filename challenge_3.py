# http://www.pythonchallenge.com/pc/def/equality.html
def challenge3():
    import re
    import urllib3
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://www.pythonchallenge.com/pc/def/equality.html')
    tempmess =(r.data).decode('utf-8')
    mess = tempmess[tempmess.find('<!--'):]
    mess = mess[:-3]
    check = ''
    i=0
    print('Challenge 3 solution is: ',end = '')

    while i < len(str(mess))-9:
        check = mess[i:i+9]
        if check.isalpha():
            if check[0].islower():
                if check[1:4].isupper():
                    if check[4].islower():
                        if check[5:8].isupper():
                            if check[8].islower():
    #                            print(check)
                                print(check[4],end='')
        i += 1
    print('')

    # OR use the re (Regular Expression) module
    solution_list = re.findall("[^A-Z]+[A-Z]{3}([a-z])[A-Z]{3}[^A-Z]+", mess)
    solution = ''.join(solution_list)
    print(f"Next url is http://www.pythonchallenge.com/pc/def/{solution}.html")




if __name__ == '__main__':
    challenge3()