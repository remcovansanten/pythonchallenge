#http://www.pythonchallenge.com/pc/def/ocr.html
#solution: http://wiki.pythonchallenge.com/level2
from generic_functions import *

def challenge2():
    import urllib3
    http = urllib3.PoolManager()
    r = http.request('GET', 'http://www.pythonchallenge.com/pc/def/ocr.html')
    source =(r.data).decode('utf-8')
    mess = source[source.find('-->'):]
    sol1 =''
    for char in mess:
        if(97 <= ord(char) <= 122):
            sol1.join(chr(ord(char)))
    solution = "".join([char for char in mess if char.isalpha()])
    print_solution('2', 'def' ,solution)

if __name__ == '__main__':
    challenge2()

