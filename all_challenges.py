from challenge_1 import *
from challenge_2 import *
from challenge_3 import *
from challenge_4 import *
from challenge_5 import *
from challenge_6 import *
from challenge_7 import *
from challenge_8 import *
from challenge_9 import *
from challenge_10 import *
from challenge_11 import *
from challenge_12 import *
from challenge_13 import *
from challenge_14 import *

def printline(x):
    for i in range(x):
        print('-',end = '')
    print('')

printline(80)
challenge1()

printline(80)
challenge2()

printline(80)
challenge3()

printline(80)
challenge4()

printline(80)
challenge5()

printline(80)
challenge6()

printline(80)
challenge7()

printline(80)
challenge8()

printline(80)
challenge9()

printline(80)
challenge10()

printline(80)
challenge11()


printline(80)
challenge12()


printline(80)
challenge13()


printline(80)
challenge14()
