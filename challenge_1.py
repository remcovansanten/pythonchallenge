#http://www.pythonchallenge.com/pc/def/map.html
#encoded = "map"
def challenge1():

    print('Challenge 1')
        
    encoded= "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
    print('solution 1')
    for char in encoded:
        if(97 <= ord(char) <= 120):
            print(chr(ord(char)+2), end = '')
        elif(121 <= ord(char) <= 122):
            print(chr(ord(char)-24), end = '')
        else:
            print(char, end = '')
    print()

    print('Solution 2')
    encoded = 'map'
    intab = "abcdefghijklmnopqrstuvwxyz"
    outtab = "cdefghijklmnopqrstuvwxyzab"
    trantab = str.maketrans(intab, outtab)
    solution = (encoded.translate(trantab))
    
    print(f"Next url is http://www.pythonchallenge.com/pc/def/{solution}.html")

if __name__ == '__main__':
    challenge1()