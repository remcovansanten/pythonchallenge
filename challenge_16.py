'''
url: http://www.pythonchallenge.com/pc/return/mozart.html
source:
<html>
<head>
  <title>let me get this straight</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<br><center>
<img src="mozart.gif"><br>
</body>
</html>


'''
from generic_functions import *
from PIL import Image


def challenge16():
    img = 'mozart.gif'
    img_path = 'resources/' + img
    DownloadFile('http://www.pythonchallenge.com/pc/return/', img)
    img = Image.open(img_path)
    width, height = img.size

    for y in range(height):
        colors = [img.getpixel((x, y)) for x in range(width)]
        start = colors.index(195)  # Get red line start position
        # Move the red line to the leftmost
        colors = colors[start:] + colors[:start]
        for x in range(width):
            img.putpixel((x, y), colors[x])  # Redraw
        img.save("resources/solution16.png")

    if __name__ == '__main__':
        print('')
        SolImg= Image.open("resources/solution16.png")
        SolImg.show()
    print_solution('16', 'return', 'romance')


if __name__ == '__main__':
    challenge16()
