'''
url: http://www.pythonchallenge.com/pc/return/uzi.html
source:
<html>
<head>
  <title>whom?</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<br><center>
<!-- he ain't the youngest, he is the second -->
<img src="screen15.jpg"><br>
</body>
</html>

<!-- todo: buy flowers for tomorrow -->

---------
What are we looking for: a specific date? or a person (whom?) -> or a birthdat (date and person combined)

Year has a burnt hole, but must be something like 1xx6
and January first must be on Thursday
Circle around the 26th, but buy flower for tomorrow. Looking for a date of Tuseday 27 January, 1xx6



'''
from generic_functions import *
import datetime



def challenge15():
    for year in range (1006, 1996, 10):
        if year % 4 == 0: #leapyear (on the calendar, February has 29 days.... saw that after three days)
            checkdate = datetime.datetime(year, 1, 27)
            #weekday: watch out in python: weekday 0 = Monday. weekday 6 = Sunday
            if checkdate.weekday() == 1: # January 27 is on Tuesday
                print(checkdate)
    
    print("hint: he ain't the youngest, he is the second")
    print('So the date must be: 1756-01-27 00:00:00')
    print('answer: https://www.onthisday.com/date/1756/january/27')
    print_solution('15' , 'return' , 'mozart')


if __name__ == '__main__':
    challenge15()