'''
url: http://www.pythonchallenge.com/pc/stuff/balloons.html
reroute to: http://www.pythonchallenge.com/pc/return/balloons.html
source:
<html>
<head>
  <title>can you tell the difference?</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<br><br>
	<center>
	<font color="gold">
	<img src="balloons.jpg" border="0"/>
<!-- it is more obvious that what you might think -->
</body>
</html>
'''
from generic_functions import *
from PIL import Image
import cv2
import numpy as np
import gzip
import difflib
from pprint import pprint


def deep_compare(actual, expected):

    if type(actual) != type(expected):
        return 'EXPECTED %s BUT ACTUAL %s' % (type(expected), type(actual))

    elif isinstance(expected, (dict, tuple, list)):
        expected_str = json.dumps(expected, indent=2, sort_keys=True)
        actual_str = json.dumps(actual, indent=2, sort_keys=True)

        delta = list(Differ().compare(
            expected_str.splitlines(),
            actual_str.splitlines()
        ))

        if sum(1 for d in delta if d[0] in ['-', '+', '?']):
            return '%s\nEXPECTED *******************************************************\n%s\nACTUAL *******************************************************\n%s' % ('\n'.join(delta), expected_str, actual_str)

    elif actual != expected:
        return 'EXPECTED %s != ACTUAL %s' % (expected, actual)

    return None


# display results of list comparison

def challenge18():
    img = 'balloons.jpg'
    img_path = 'resources/' + img
    DownloadFile('http://www.pythonchallenge.com/pc/return/', img)
    img = Image.open(img_path)
    width, height = img.size
    print(f'Width: {width}, heighth: {height}')

    # img = cv2.imread(img_path)
    # b,g,r=cv2.split(img)
    # cv2.imshow('b',b)
    # cv2.imshow('g',g)
    # cv2.imshow('r',r)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # width_cutoff = width // 2
    # left_img = img[:,:width_cutoff]
    # right_img = img[:,width_cutoff:]
    # cv2.imshow('left',left_img)
    # cv2.imshow('right', right_img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # print("hmmm, this is getting nowhere")
    print("look at the hint...<!-- it is more obvious that what you might think -->")
    print("literally the difference is brightness")
    print("So let's try:  http://www.pythonchallenge.com/pc/return/brightness.html")
    print("page looks the same, but different comment in source: <!-- maybe consider deltas.gz -->")
    gz_url = "http://www.pythonchallenge.com/pc/return/deltas.gz"
    print(f" download the deltas.gz file from: {gz_url}")
    zip_file = DownloadFile(
        "http://www.pythonchallenge.com/pc/return/", 'deltas.gz')
    with gzip.open(zip_file, 'rb') as f:
        file_content = f.read().decode()
        print(file_content)

    data = gzip.open("resources/deltas.gz")
    d1, d2 = [], []
    for line in data:
        d1.append(line[:53].decode()+"\n")
        d2.append(line[56:].decode())

    compare = difflib.Differ().compare(d1, d2)
    pprint(compare)

    f = open("resources/sol18-1.png", "wb")
    f1 = open("resources/sol18-2.png", "wb")
    f2 = open("resources/sol18-3.png", "wb")

    for line in compare:
        print(f"Line: {line}")
        bs = bytes([int(o, 16) for o in line[2:].strip().split(" ") if o])
        print(f"bs: {bs}")
        if line[0] == '+':
            f1.write(bs)
        elif line[0] == '-':
            f2.write(bs)
        else:
            f.write(bs)

    f.close()
    f1.close()
    f2.close()

    if __name__ == '__main__':
        print('')
        print_solution('18', 'hex', 'bin')


if __name__ == '__main__':
    challenge18()
