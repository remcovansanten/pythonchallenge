import requests
from requests.auth import HTTPBasicAuth



def DownloadFile(url_part, filename):
    img_path = 'resources/'+ filename
    username = 'huge'
    password = 'file'
    url = url_part + filename
    response = requests.get(url, auth=(username, password))
    if response.status_code == 200:
        file = open(img_path, "wb")
        file.write(response.content)
        file.close()
    else:
        print(f'error. Status code = {response.status_code}')
    return img_path

def print_solution(challenge_nbr, url_part,solution):
        print('----------------------------------------------------------------------------')
        print(f'\nChallenge {challenge_nbr} solution is: {solution}')
        print(f"Next url is http://www.pythonchallenge.com/pc/{url_part}/{solution}.html")
