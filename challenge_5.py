# http://www.pythonchallenge.com/pc/def/peak.html
'''<html>
<head>
  <title>peak hell</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<center>
<img src="peakhell.jpg"/>
<br><font color="#c0c0ff">
pronounce it
<br>
<peakhell src="banner.p"/>
</body>
</html>

<!-- peak hell sounds familiar ? -->
'''
def challenge5():
    import urllib.request, pickle
    pfile = ('resources/banner.p')
    url = 'http://www.pythonchallenge.com/pc/def/banner.p'
    urllib.request.urlretrieve(url, pfile)
    print('Challenge 5 solution is: ')
    with open(pfile, 'rb') as pickle_file:
        content = pickle.load(pickle_file, fix_imports=True, encoding='utf-8', errors='strict')
    for line in content:
        for chars in line:
            print(chars[0]*chars[1], end = "")
        print()

    print(f"Next url is http://www.pythonchallenge.com/pc/def/channel.html")

if __name__ == '__main__':
    challenge5()