"""
http://www.pythonchallenge.com/pc/return/italy.html

page source:
----
<html>
<head>
  <title>walk around</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<center>
<img src="italy.jpg"><br>
<br>

<!-- remember: 100*100 = (100+99+99+98) + (...  -->

<img src="wire.png" width="100" height="100">

</body>
</html>
----
"""
import numpy as np
import cv2 as cv2

img_name_in = 'resources/wire.png'
img_name_out = 'resources/sol_14.png'
img_in = cv2.imread(img_name_in)
img_out = np.ones((100,100,3),np.uint8)
pixel_nr = 0   # image in pixel to read

def get_pixel_value():
    global pixel_nr
    px = img_in[0,pixel_nr]
    pixel_nr += 1
    return px

def challenge14():
    # need to create a spiral (I think) with
    # 99 right, 99 down, 99 left, 99 up
    # 98 right, 98 down, 98 left, 98 up

    wh=100      #img_out pixel width/height
    while pixel_nr <10000:
        for i in range(100-wh,wh-1): # horizontal to right
            img_out[100-wh,i] = get_pixel_value()
        for i in range(100-wh,wh-1): # right side down
            img_out[i, wh-1] = get_pixel_value()
        for i in range(wh-1,100-wh, -1): # down side left
            img_out[wh-1, i] = get_pixel_value()
        for i in range(wh-1,100-wh, -1): # down side left
            img_out[i, 100-wh] = get_pixel_value()
        wh -= 1 # 4 sides done, so one line (spiral) to the inside
    #Spiral done, let's show the end-result
    cv2.imwrite(img_name_out, img_out)

    if __name__ == '__main__':
        cv2.imshow('image',img_out)
        cv2.waitKey(0)
        cv2.destroyAllWindows()    

        solution = 'cat'
        print(f'Challenge 14 solution is: {solution}')
        print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")
        print('\n.. not quite there yet ...')
        print("it says: and its name is uzi. you'll hear from him later.") 
        solution = 'uzi'
        print(f'Challenge 14 firnal solution is: {solution}')
        print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")
 

if __name__ == '__main__':
    challenge14()