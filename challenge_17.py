'''
url: http://www.pythonchallenge.com/pc/return/romance.html
source:
<html>
<head>
  <title>eat?</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<br><br>
	<center>
	<font color="gold">
	<img src="cookies.jpg" border="0"/>
</body>
</html>

'''
from generic_functions import *
import requests
from PIL import Image
from urllib.parse import unquote_plus, unquote_to_bytes
import re, bz2
import xmlrpc.client
from urllib.request import Request, urlopen
from urllib.parse import quote_plus

username = 'huge'
password = 'file'

def challenge17():
    img_name = 'cookies.jpg'
    img_path = 'resources/' + img_name
    DownloadFile('http://www.pythonchallenge.com/pc/return/', img_name)
    img = Image.open(img_path)
    #img.show()

    print("The image with the saw comes from challenge 4...")
    print("So let's go back to challenge 4 and see if there's a cookie")
    r = requests.get('http://www.pythonchallenge.com/pc/def/linkedlist.php')
    if r.status_code == 200:
        for c in r.cookies:
            print(c.name, c.value)
    else:
        print(f"Error with status code: {r.status_code}")

    #redo challewnge 4, but now with busynothing
    num = '12345'
    info = ''
    while(True):
        h = urlopen('http://www.pythonchallenge.com/pc/def/linkedlist.php?busynothing='+num)
        raw = h.read().decode("utf-8")
        print(raw)
        cookie = h.getheader("Set-Cookie")
        info += re.search('info=(.*?);', cookie).group(1)
        match = re.search('the next busynothing is (\d+)', raw)
        if match == None:
            break
        else:
            num = match.group(1)
    res = unquote_to_bytes(info.replace("+", " "))
    print(res)
    print(bz2.decompress(res).decode())

    print("\n let's call Leopold (father of Mozart)")
    print("we re-use the code from challenge 13")
    with xmlrpc.client.ServerProxy("http://www.pythonchallenge.com/pc/phonebook.php") as proxy:
        print("Leopold's phone number is: %s" % str(proxy.phone("Leopold"))) 

    url = "http://www.pythonchallenge.com/pc/stuff/violin.php"
    cookies_dict = {"info": "the flowers are on their way"}
    #req = Request(url, headers = { "Cookie": "info=" + quote_plus(msg)})
    response = requests.get(url, cookies=cookies_dict)
    print(response.content)
    


    if __name__ == '__main__':
        print('')
        print_solution('17', 'return', 'balloons')


if __name__ == '__main__':
    challenge17()
