'''
url: http://www.pythonchallenge.com/pc/return/5808.html

source:
<html>
<head>
  <title>odd even</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<br><br>
	<center>
	<img src="cave.jpg" width="640" height="480" border="0"/>
	<br>
	<br>
	<font color="gold" size="+1"></font>
</body>
</html>

------
what I dis? see: https://techtutorialsx.com/2019/04/13/python-opencv-converting-image-to-black-and-white/
'''
def challenge11():
  import requests
  from requests.auth import HTTPBasicAuth
  import cv2

  img_path = 'resources/cave.jpg'
  username = 'huge'
  password = 'file'
  url = 'http://www.pythonchallenge.com/pc/return/cave.jpg'
  response = requests.get(url, auth=(username, password))
  if response.status_code == 200:
      file = open(img_path, "wb")
      file.write(response.content)
      file.close()
  else:
      print(f'error. Status code = {response.status_code}')

  originalImage = cv2.imread(img_path, cv2.IMREAD_UNCHANGED )
  grayImage = cv2.cvtColor(originalImage, cv2.COLOR_BGR2GRAY)
  (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 25, 255, cv2.THRESH_BINARY)

  if __name__ == '__main__':
    cv2.imshow('Original image',originalImage)
    #cv2.imshow('Gray image', grayImage)
    cv2.imshow('Black white image', blackAndWhiteImage)
    cv2.waitKey(0) 
    cv2.destroyAllWindows()
  cv2.imwrite('resources/solution11.jpg', blackAndWhiteImage)


  print(f'After converting the image to B/W with low treshold value, it shows a text')
  solution = 'evil'
  print(f'Challenge 10 solution is: {solution}')
  print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")

if __name__ == '__main__':
    challenge11()