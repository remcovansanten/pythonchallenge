'''
url: http://www.pythonchallenge.com/pc/return/evil.html
source:
<html>
<head>
  <title>dealing evil</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
<center>
<img src="evil1.jpg"><br>
</body>
</html>


'''
from generic_functions import *
import cv2


def challenge12():
    gfxFile = open("resources/evil2.gfx", "rb").read()
    # print(gfxFile)
    print(len(gfxFile))

    for i in range(5):
        fname = 'resources/sol12-' + str(i) +'.jpg'
        open(fname ,'wb').write(gfxFile[i::5])

    if __name__ == '__main__':
        solution = 'disproportional'
        print(f'Challenge 12 solution is: {solution}')
        print(f"\nNext url is http://www.pythonchallenge.com/pc/return/{solution}.html")


if __name__ == '__main__':
    challenge12()