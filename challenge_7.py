# http://www.pythonchallenge.com/pc/def/oxygen.html
'''
<html>
<head>
  <title>smarty</title>
  <link rel="stylesheet" type="text/css" href="../style.css">
</head>
<body>
	<br><br>
	<center>
	<img src="oxygen.png"/>
</body>
</html>

Use opencv for imageprocessing and grayscales with:
code = img[45, x]

'''
def challenge7():
    import cv2
    import urllib.request
    import datetime
    url = 'http://www.pythonchallenge.com/pc/def/oxygen.png'
    urllib.request.urlretrieve(url, 'resources/7-oxygen.png')
    img = cv2.imread('resources/7-oxygen.png')
    solution = []
    print('------')
    print('Part 1: ',end = '')
    for x in range(1,605,7):
        code = img[45, x]
        #print('Pixel: 45,' + str(x) + ' has code:' + str(code[0])+ ' this is char: '+ chr(code[0]) )
        solution.append(chr(code[0]))
    print("".join(solution))
    print('------')
    print('Part 2')
    array = [105, 110, 116, 101, 103, 114, 105, 116, 121]
    print('Challenge 7 solution is: ',end = '')
    solution = listToChrString(array)
    print(solution)
    print(f"Next url is http://www.pythonchallenge.com/pc/def/{solution}.html")

# Function to convert a list to string 
def listToChrString(s): 
    # initialize an empty string
    str1 = "" 
    # traverse in the string  
    for ele in s: 
        str1 += chr(ele)  
    # return string  
    return str1 

if __name__ == '__main__':
	challenge7()